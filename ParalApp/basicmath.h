#ifndef BASIC_MATH_H_
#define BASIC_MATH_H_

int basic_addition(int addend1, int addend2);

int basic_substraction(int minuen, int subtrahend);

int basic_multiplication(int factor1, int factor2);

double basic_division(double dividend, double divisor);

double basic_square_root(double radicand);

#endif
