#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
// tamaños de los vectores
#define SIZE_1 10
#define SIZE_2 50
#define SIZE_3 150
// escalares que multiplican los vectores
#define SCALAR_1 2.2f
#define SCALAR_2 6.8f
#define SCALAR_3 7.7f
//divisor del numero random
#define DIVISOR 100000
#define PRT_DIV "-------------------------------------------------------------------------------------------------------------"

void SAXPY_ser (float * x, float * y, float a, int vec_s);

void SAXPY_par (float * x, float * y, float a, int vec_s);
// funcion para llenar los vectores con valores random
void fill_arrays(float * v1, float * v2, float * v3);

int main(int argc, char const *argv[]) {

  time_t t;
  srand((unsigned)time(&t));

  double time_1, time_2, time_3, time_4, time_5, time_6;
  float x1[SIZE_1], x2[SIZE_2], x3[SIZE_3];
  float y1[SIZE_1], y2[SIZE_2], y3[SIZE_3];

  fill_arrays(x1, x2, x3);
  fill_arrays(y1, y2, y3);

  time_1 = omp_get_wtime();
  SAXPY_ser(x1, y1, SCALAR_1, SIZE_1);
  time_1 = omp_get_wtime() - time_1;

  time_2 = omp_get_wtime();
  SAXPY_par(x1, y1, SCALAR_1, SIZE_1);
  time_2 = omp_get_wtime() - time_2;

  printf("El tiempo serial con el vector de longitud %d es de %f y el tiempo paralelo es %f\n", SIZE_1, time_1, time_2);
  printf("%s\n", PRT_DIV);


  time_3 = omp_get_wtime();
  SAXPY_ser(x2, y2, SCALAR_2, SIZE_2);
  time_3 = omp_get_wtime() - time_3;

  time_4 = omp_get_wtime();
  SAXPY_par(x2, y2, SCALAR_2, SIZE_2);
  time_4 = omp_get_wtime() - time_4;

  printf("El tiempo serial con el vector de longitud %d es de %f y el tiempo paralelo es %f\n", SIZE_2, time_3, time_4);
  printf("%s\n", PRT_DIV);


  time_5 = omp_get_wtime();
  SAXPY_ser(x3, y3, SCALAR_3, SIZE_3);
  time_5 = omp_get_wtime() - time_5;

  time_6 = omp_get_wtime();
  SAXPY_par(x3, y3, SCALAR_3, SIZE_3);
  time_6 = omp_get_wtime() - time_6;

  printf("El tiempo serial con el vector de longitud %d es de %f y el tiempo paralelo es %f\n", SIZE_3, time_5, time_6);
  printf("%s\n", PRT_DIV);

  return 0;
}

void SAXPY_ser(float * x, float * y, float a, int vec_s){
  for(int i = 0; i < vec_s; i++){
    y[i] = a * (x[i]) + y[i];
  }
}

void SAXPY_par(float * x, float * y, float a, int vec_s){
  #pragma omp parallel for
  for(int i = 0; i < vec_s; i++){
    y[i] = a * (x[i]) + y[i];
  }
}

void fill_arrays (float * v1, float * v2, float * v3){

  for(int i = 0; i < SIZE_1; i++){
    v1[i] = (float)(rand()/DIVISOR);
  }

  for(int i = 0; i < SIZE_1; i++){
    v2[i] = (float)(rand()/DIVISOR);
  }

  for(int i = 0; i < SIZE_1; i++){
    v3[i] = (float)(rand()/DIVISOR);
  }
}
